package com.pancaker.calculator.controller;

import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/calculate")
public class CalculatorController {

    @PostMapping(value = "/add")
    public OperationResponse getAdditionResult(@RequestBody OperationRequest parameters) {
        var result = parameters.getLeftParameter() + parameters.getRightParameter();
        log.info("results: {} + {} = {}",
                parameters.getLeftParameter(),
                parameters.getRightParameter(),
                result);

        return OperationResponse.builder()
                .type(OperationResponse.OperationType.ADD)
                .result(result)
                .generationTime(LocalDateTime.now())
                .build();
    }
}
