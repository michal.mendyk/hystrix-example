package com.pancaker.proxy.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Slf4j
@Service
public class OperationHystrixCollapser {

    private final OperationRestService restService;

    public OperationHystrixCollapser(OperationRestService restService) {
        this.restService = restService;
    }

    /**
     * Asynchronous Execution
     */
    @HystrixCollapser(batchMethod = "additionCollapsed")
    public Future<OperationResponse> addAsync(OperationRequest request) {
        return null;
    }

    /**
     * Reactive Execution
     */
    @HystrixCollapser(batchMethod = "additionCollapsed")
    public Observable<OperationResponse> addReact(OperationRequest request) {
        return null;
    }

    @SuppressWarnings("unused")
    @HystrixCommand(fallbackMethod = "randAdditionFallbackCollapsed")
    public List<OperationResponse> additionCollapsed(List<OperationRequest> requests) {
        var responses = new ArrayList<OperationResponse>();
        for (var request : requests) {
            responses.add(restService.add(request));
        }

        return responses;
    }

    @SuppressWarnings("unused")
    public List<OperationResponse> randAdditionFallbackCollapsed(List<OperationRequest> requests) {
        var responses = new ArrayList<OperationResponse>();
        for (var request : requests) {
            responses.add(OperationResponse.builder()
                    .type(OperationResponse.OperationType.ERROR)
                    .build());
        }

        return responses;
    }
}
