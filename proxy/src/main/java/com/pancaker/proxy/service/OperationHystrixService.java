package com.pancaker.proxy.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.Future;

@Slf4j
@Service
@DefaultProperties(defaultFallback = "localAdd")
public class OperationHystrixService {

    private final OperationRestService restService;

    public OperationHystrixService(OperationRestService restService) {
        this.restService = restService;
    }

    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse addWithHystrix(OperationRequest request) {
        log.info("With Hystrix");
        return restService.add(request);
    }

    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse addWithHystrixRandException(OperationRequest request) {
        log.info("With rand exceptions");
        return restService.addWithRandomFailure(request);
    }


    @HystrixCommand(ignoreExceptions = {RuntimeException.class},
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
            })
    public OperationResponse addWithHystrixRandExceptionIgnored(OperationRequest request) {
        log.info("With rand exceptions ignored by command");
        return restService.addWithRandomFailure(request);
    }


    @SneakyThrows
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse addAlwaysTooSlow(OperationRequest request) {
        log.info("Always too slow");
        Thread.sleep(3000);
        return restService.add(request);
    }

    @SneakyThrows
    @HystrixCommand(fallbackMethod = "randAsyncResult", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse addAlwaysTooSlowDifferentFallBack(OperationRequest request) {
        log.info("Always too slow");
        Thread.sleep(3000);
        return restService.add(request);
    }


    @SuppressWarnings("unused")
    private OperationResponse localAdd() {
        log.info("Fallback");
        return OperationResponse.builder().type(OperationResponse.OperationType.ERROR).build();
    }

    @SuppressWarnings("unused")
    Future<OperationResponse> randAsyncResult() {
        return new AsyncResult<OperationResponse>() {
            @Override
            public OperationResponse invoke() {
                Random random = new Random();
                return OperationResponse.builder()
                        .result(random.nextFloat() * 1000)
                        .type(OperationResponse.OperationType.ERROR).build();
            }
        };
    }
}
