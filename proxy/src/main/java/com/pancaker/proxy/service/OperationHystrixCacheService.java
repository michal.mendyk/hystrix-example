package com.pancaker.proxy.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheKey;
import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OperationHystrixCacheService {

    private final OperationRestService operationRestService;

    public OperationHystrixCacheService(OperationRestService operationRestService) {
        this.operationRestService = operationRestService;
    }

    @CacheResult
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse withManyKeys(Float in, Float tmp) {
        return operationRestService.addWithRandomFailure(in, tmp);
    }

    @CacheResult
    @HystrixCommand(commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")
    })
    public OperationResponse withManyKeysOneCacheKey(@CacheKey Float in, Float tmp) {
        return operationRestService.addWithRandomFailure(in, tmp);
    }

    @CacheResult
    @HystrixCommand(ignoreExceptions = {RuntimeException.class},
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10")
            })
    public OperationResponse withManyKeysFromObject(OperationRequest request) {
        return operationRestService.addWithRandomFailure(request.getLeftParameter(), request.getRightParameter());
    }


    @CacheResult
    @HystrixCommand(ignoreExceptions = {RuntimeException.class},
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10")
            })
    public OperationResponse withManyKeysOneCacheKeyFromObject(@CacheKey("leftParameter") OperationRequest request) {
        return operationRestService.addWithRandomFailure(request.getLeftParameter(), request.getRightParameter());
    }
}
