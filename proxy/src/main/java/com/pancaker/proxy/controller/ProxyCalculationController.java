package com.pancaker.proxy.controller;

import com.pancaker.common.dto.OperationResponse;
import com.pancaker.proxy.service.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/proxy")
public class ProxyCalculationController {

    //region init
    private final OperationService operationService;

    public ProxyCalculationController(OperationService operationService) {
        this.operationService = operationService;
    }
    //endregion

    //region with out
    @PostMapping(value = "/add")
    public OperationResponse getAdditionResult() {
        return operationService.withoutAnyErrorHandling();
    }

    @PostMapping(value = "/add-with-chance-random-exception")
    public OperationResponse addWithChanceRandomException() {
        return operationService.withoutAnyErrorRandomException();
    }
    //endregion
}
